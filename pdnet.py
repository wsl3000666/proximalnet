# pylint: skip-file
import sys, os
import argparse
import mxnet as mx
import numpy as np
import math
import logging

def psnr(src_im, tgt_im):
    mse = ((src_im - tgt_im) ** 2).mean(axis = None)
    return 20*math.log10(255/math.sqrt(mse))

class prox_l1(mx.operator.NumpyOp):
    def __init__(self):
        super(prox_l1, self).__init__(True)
    
    def list_arguments(self):
        return ['data', 'tau']
    
    def list_outputs(self):
        return ['output']
    
    def infer_shape(self, in_shape):
        x_shape = in_shape[0]
        lambda_shape = in_shape[1]
        output_shape = in_shape[0]
        return [x_shape, lambda_shape], [output_shape]
    
    def forward(self, in_data, out_data):
        x = in_data[0]
        tau = in_data[1][0]
        # print(tau)
        # print(x)
        y = out_data[0]
        y[:] = x + np.clip(-x, -tau, tau)
        # print(y)
    
    def backward(self, out_grad, in_data, out_data, in_grad):
        dx = in_grad[0]
        dtau = in_grad[1]
        dy = out_grad[0]
        x = in_data[0]
        tau = in_data[1] 
        y = out_data[0]
        dx[:] = ((x < tau) | (x >= -tau)) * (1.0)
        # dx = ((x >= tau) | (x < -tau)) * (-1.0)
        dtau = 0
        
class proj_nd(mx.operator.NumpyOp):
    def __init__(self):
        super(proj_nd, self).__init__(True)
    
    def list_arguments(self):
        return ['data']
        # return ['data', 'tau']
    
    def list_outputs(self):
        return ['output']
    
    def infer_shape(self, in_shape):
        x_shape = in_shape[0]
        output_shape = in_shape[0]
        return [x_shape], [output_shape]
    
    def forward(self, in_data, out_data):
        x = in_data[0]
        y = out_data[0]
        y[:] = np.clip(x, -1, 1)
        # return P / nP[...,np.newaxis]
        # print(y)
    
    def backward(self, out_grad, in_data, out_data, in_grad):
        dx = in_grad[0]
        x = in_data[0]
        y = out_data[0]
        dx[:] = ((x >= 1) | (x < -1)) * (1.0)


def iter_factory(x, p, y, tau, sigma, theta, clambda, idx, params, n_filters, k_size):
    lt = clambda * tau
    my_proj = proj_nd()
    p_size = (k_size - 1) / 2
    weight = mx.symbol.Variable('fact%d_weight' % idx)
    conv1 = mx.symbol.Convolution(data=x, weight=weight, no_bias = True, kernel=(k_size, k_size), pad=(p_size, p_size), num_filter=n_filters, name = "conv%d" % idx)
    # w_conv1 = mx.symbol.Convolution(data=conv1, no_bias = True, kernel=(1, 1), pad=(0, 0), num_filter=1, name = "sigma_conv%d" % idx)
    p = mx.symbol.minimum(p + sigma * conv1, 1.0)
    p = mx.symbol.maximum(p, -1.0)
    # p = my_proj(data=p + sigma * conv1, name = "proj%d" % idx)
    decov1 = mx.symbol.Deconvolution(data=p, no_bias = True, kernel=(k_size, k_size), stride = (1, 1), pad = (p_size, p_size), num_filter=1, name = "decov%d" % idx)
    # decov1 = mx.symbol.Deconvolution(data=p, weight=weight, no_bias = True, kernel=(3, 3), stride = (1, 1), pad = (1, 1), num_filter=1, name = "decov%d" % idx)
    # grad = mx.symbol.Convolution(data=decov1, weight=params.S, no_bias = True, kernel=(1, 1), stride = (1, 1), num_filter=1, name = "sum%d" % idx)
    
    # lt = mx.symbol.Variable('lt%d_lambda' % idx)
    theta_sym = mx.symbol.Variable('theta%d' % idx)
    # tau_sym = mx.symbol.Variable('tau%d' % idx)
    # lt_inv = 1 / (1.0 + lt) 
    # tau_grad = mx.symbol.Convolution(data=decov1, weight = tau_sym, no_bias = True, kernel=(1, 1), pad=(0, 0), num_filter=1, name = "tau_decov%d" % idx)
    # w_x1 = mx.symbol.Convolution(data=x, no_bias = True, kernel=(1, 1), pad=(0, 0), num_filter=1, name = "weighted_decov%d" % idx)
    # lt_y = mx.symbol.Convolution(data=y, weight = lt, no_bias = True, kernel=(1, 1), stride = (1, 1), num_filter=1, name = "sum_y%d" % idx)
    # x1 = (x - w_decov1 + lt * y) / (1.0 + lt)
    x1 = (x - tau * decov1 + lt * y) / (1.0 + lt)
    # x1 = (x - tau * grad + lt * y) / (1.0 + lt)
    # x1 = x + tau_grad + lt_y
    # x1 = x - tau * decov1 + w_y
    # x1 = mx.symbol.Convolution(data=x1, weight = lt_inv, no_bias = True, kernel=(1, 1), stride = (1, 1),  num_filter=1, name = "sum_x%d" % idx)
    delta_x = mx.symbol.Convolution(data=x1 - x, weight = theta_sym, no_bias = True, kernel=(1, 1), pad=(0, 0), num_filter=1, name = "sum_theta_grad%d" % idx)
    x = x1 + delta_x 
    return (x, p)
    
class params_struct:
    weight = mx.symbol.Variable('weight')

    
def primal_dual_net(n_filters, n_blocks, tau, clambda, ksize):    
    params = params_struct()
    L2 = 8.0
    # sigma = 5 
    sigma = 1.0 / (L2*tau)
    theta = 1.0
    # clambda = 20.0
    # x = mx.symbol.Variable('x')
    # p = mx.symbol.Variable('p')
    y = mx.symbol.Variable('data')
    x = y
    p = mx.symbol.Convolution(data=x, weight=params.weight, no_bias = True, kernel=(3, 3), pad=(1, 1), num_filter=n_filters, name = "p_init")
    # x, p = iter_factory(x, p, y, tau, sigma, theta, clambda, 1, params, n_filters, ksize)
    # loss7 = mx.symbol.LinearRegressionOutput(data=x, name = "loss7", grad_scale = 0.1)
    # x, p = iter_factory(x, p, y, tau, sigma, theta, clambda, 1, params, n_filters, ksize)
    # loss6 = mx.symbol.LinearRegressionOutput(data=x, name = "loss6", grad_scale = 0.1)
    x, p = iter_factory(x, p, y, tau, sigma, theta, clambda, 1, params, n_filters, ksize)
    loss5 = mx.symbol.LinearRegressionOutput(data=x, name = "loss5", grad_scale = 0.4)
    x, p = iter_factory(x, p, y, tau, sigma, theta, clambda, 2, params, n_filters, ksize)
    loss4 = mx.symbol.LinearRegressionOutput(data=x, name = "loss4", grad_scale = 0.5)
    x, p = iter_factory(x, p, y, tau, sigma, theta, clambda, 3, params, n_filters, ksize)
    loss3 = mx.symbol.LinearRegressionOutput(data=x, name = "loss3", grad_scale = 0.6)
    x, p = iter_factory(x, p, y, tau, sigma, theta, clambda, 4, params, n_filters, ksize)
    loss2 = mx.symbol.LinearRegressionOutput(data=x, name = "loss2", grad_scale = 0.8)
    x, p = iter_factory(x, p, y, tau, sigma, theta, clambda, 5, params, n_filters, ksize)
    loss1 = mx.symbol.LinearRegressionOutput(data=x, name = "loss1", grad_scale = 1.0)
    # loss_combine = mx.symbol.Group([loss7, loss6, loss5, loss4, loss3, loss2, loss1])
    loss_combine = mx.symbol.Group([loss5, loss4, loss3, loss2, loss1])
    net = loss_combine 
    return net
