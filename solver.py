# solver file
import numpy as np
import sys, os
import mxnet as mx
import time
import math
import logging
import scipy.misc
from collections import namedtuple
from mxnet import optimizer as opt
from mxnet.optimizer import get_updater
from mxnet import metric
from mxnet.io import DataIter

# Parameter to pass to batch_end_callback
BatchEndParam = namedtuple('BatchEndParams', ['epoch', 'nbatch', 'eval_metric'])
class Solver(object):
    def __init__(self, symbol, ctx=None,
                 begin_epoch=0, num_epoch=None,
                 arg_params=None, aux_params=None,
                 optimizer='adam', **kwargs):
        self.symbol = symbol
        if ctx is None:
            ctx = mx.cpu()
        self.ctx = ctx
        self.begin_epoch = begin_epoch
        self.num_epoch = num_epoch
        self.arg_params = arg_params
        self.aux_params = aux_params
        self.optimizer = optimizer
        self.kwargs = kwargs.copy()

    def fit(self, train_data, eval_data=None,
            eval_metric='rmse',
            grad_req='write',
            epoch_end_callback=None,
            batch_end_callback=None,
            kvstore='local',
            data_name='data',
            label_name='label',
            log_file='train.log',
            logger=None):
       
        head = '%(asctime)-15s %(message)s'
        logger = logging.getLogger()
        handler = logging.FileHandler(log_file)
        formatter = logging.Formatter(head)
        handler.setFormatter(formatter)
        logger.addHandler(handler)
        logger.setLevel(logging.INFO)
        logger.info('start with arguments %s', str(self.ctx))
        
        print('Start training with %s' % str(self.ctx))
        arg_shapes, out_shapes, aux_shapes = self.symbol.infer_shape(data=train_data.provide_data[0][1])
        arg_names = self.symbol.list_arguments()
        if grad_req != 'null':
            self.grad_params = {}
            for name, shape in zip(arg_names, arg_shapes):
                if not (name.endswith('data') or name.endswith('label')):
                    self.grad_params[name] = mx.nd.zeros(shape, self.ctx)
        else:
            self.grad_params = None
        aux_names = self.symbol.list_auxiliary_states()
        self.aux_params = {k : nd.zeros(s) for k, s in zip(aux_names, aux_shapes)}
        input_names = [data_name, label_name]
        self.optimizer = opt.create(self.optimizer, rescale_grad=(1.0/train_data.get_batch_size()), **(self.kwargs))
        self.updater = get_updater(self.optimizer)
        eval_metric = metric.create(eval_metric)
        # begin training
        for epoch in range(self.begin_epoch, self.num_epoch):
            nbatch = 0
            train_data.reset()
            eval_metric.reset()
            for data in train_data:
                nbatch += 1
                label_shape = data[label_name].shape
                self.arg_params[data_name] = mx.nd.array(data[data_name], self.ctx)
                self.arg_params['loss1_label'] = mx.nd.array(data[label_name], self.ctx)                
                self.arg_params['loss2_label'] = mx.nd.array(data[label_name], self.ctx)                
                self.arg_params['loss3_label'] = mx.nd.array(data[label_name], self.ctx)                
                self.arg_params['loss4_label'] = mx.nd.array(data[label_name], self.ctx)                
                self.arg_params['loss5_label'] = mx.nd.array(data[label_name], self.ctx)                
                # self.arg_params['loss6_label'] = mx.nd.array(data[label_name], self.ctx)                
                # self.arg_params['loss7_label'] = mx.nd.array(data[label_name], self.ctx)                
                # self.arg_params[label_name] = mx.nd.array(data[label_name].reshape(label_shape[0], \
                #     label_shape[1]*label_shape[2]), self.ctx)
                output_names = self.symbol.list_outputs()
                self.exector = self.symbol.bind(self.ctx, self.arg_params,
                                args_grad=self.grad_params,
                                grad_req=grad_req,
                                aux_states=self.aux_params)
                assert len(self.symbol.list_arguments()) == len(self.exector.grad_arrays)
                update_dict = {name: nd for name, nd in zip(self.symbol.list_arguments(), \
                    self.exector.grad_arrays) if nd}
                output_dict = {}
                output_buff = {}
                for key, arr in zip(self.symbol.list_outputs(), self.exector.outputs):
                    output_dict[key] = arr
                    output_buff[key] = mx.nd.empty(arr.shape, ctx=mx.cpu())
                self.exector.forward(is_train=True)
                for key in output_dict:
                    output_dict[key].copyto(output_buff[key])
                self.exector.backward()
                for key, arr in update_dict.items():
                    self.updater(key, arr, self.arg_params[key])
                pred_shape = self.exector.outputs[0].shape
                label = mx.nd.array(data[label_name])
                # print(label_shape, output_buff['l2_loss_output'])
                pred = mx.nd.array(output_buff['loss1_output'].asnumpy())
                # print(pred.asnumpy().shape, label.asnumpy().shape)
                if ((nbatch % 50) == 1):
                    # print(label_shape)
                    data_u = self.arg_params[data_name].asnumpy()[0,0,:,:].reshape(pred.shape[2], pred.shape[3])
                    pred_u = pred.asnumpy()[0,0,:,:].reshape(pred.shape[2], pred.shape[3])
                    label_u = label.asnumpy()[0,0,:,:].reshape(label.shape[2], label.shape[3])
                    scale = max(abs(pred_u.max()), abs(label_u.max()))
                    result = np.concatenate((data_u/scale, pred_u/scale, label_u/scale), axis=0)
                    scipy.misc.imsave("pred%d.png" % nbatch, result)
                # scipy.misc.imsave("label%d.png" % epoch,
                #                   label.asnumpy()[0, 0, :, :].reshape(label.shape[2], label.shape[3]))
                # print(label.shape, pred.shape)
                # label = mx.nd.array(data[label_name].reshape(label_shape[0], label_shape[1], label_shape[2]*label_shape[3]))
                # pred = mx.nd.array(output_buff["l2_loss_label"].asnumpy().reshape(pred_shape[0], \
                #     pred_shape[1], pred_shape[2]*pred_shape[3]))
                eval_metric.update([label], [pred])
                name, value = eval_metric.get()
                value= 20*math.log10(1/(value))
                name = 'psnr'
                print("Batch[%d] Train-%s=%f" % (nbatch, name, value))
                logger.info("Batch[%d] Train-%s=%f" ,nbatch, name, value)
                self.exector.outputs[0].wait_to_read()
                batch_end_params = BatchEndParam(epoch=epoch, nbatch=nbatch, eval_metric=eval_metric)
                batch_end_callback(batch_end_params)
            if epoch_end_callback != None:
                epoch_end_callback(epoch, self.symbol, self.arg_params, self.aux_params)
            name, value = eval_metric.get()
            value= 20*math.log10(1/(value))
            name = 'psnr'
            logger.info("                     --->Epoch[%d] Train-%s=%f", epoch, name, value)
            print("                     --->Epoch[%d] Train-%s=%f" % (epoch, name, value))
            # evaluation
            if eval_data:
                logger.info(" in eval process...")
                print(" in eval process...")
                nbatch = 0
                eval_data.reset()
                eval_metric.reset()
                for data in eval_data:
                    nbatch += 1
                    label_shape = data[label_name].shape
                    self.arg_params[data_name] = mx.nd.array(data[data_name], self.ctx)
                    self.arg_params['loss1_label'] = mx.nd.array(data[label_name], self.ctx)                
                    self.arg_params['loss2_label'] = mx.nd.array(data[label_name], self.ctx)                
                    self.arg_params['loss3_label'] = mx.nd.array(data[label_name], self.ctx)                
                    self.arg_params['loss4_label'] = mx.nd.array(data[label_name], self.ctx)                
                    self.arg_params['loss5_label'] = mx.nd.array(data[label_name], self.ctx)                
                    exector = self.symbol.bind(self.ctx, self.arg_params,
                                    args_grad=self.grad_params,
                                    grad_req=grad_req,
                                    aux_states=self.aux_params)
                    cpu_output_array = mx.nd.zeros(exector.outputs[0].shape)
                    exector.forward(is_train=False)
                    exector.outputs[0].copyto(cpu_output_array)
                    pred_shape = cpu_output_array.shape
                    label = mx.nd.array(data[label_name])
                    pred = mx.nd.array(cpu_output_array.asnumpy())
                    
                    # label = mx.nd.array(data[label_name].reshape(label_shape[0], \
                    #     label_shape[1]*label_shape[2]))
                    # pred = mx.nd.array(cpu_output_array.asnumpy().reshape(pred_shape[0], \
                    #     pred_shape[1], pred_shape[2]*pred_shape[3]))
                    eval_metric.update([label], [pred])
                    exector.outputs[0].wait_to_read()
            name, value = eval_metric.get()
            value= 20*math.log10(1/(value))
            name = 'psnr'
            logger.info('batch[%d] Validation-%s=%f', nbatch, name, value)
            print('batch[%d] Validation-%s=%f' % (nbatch, name, value))
